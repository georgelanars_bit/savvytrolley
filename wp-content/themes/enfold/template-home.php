<?php
    /*
    Template Name: Homepage
    */

    if ( !defined('ABSPATH') ){ die(); }

    global $avia_config;

    get_header();

    echo avia_title(array('title' => avia_which_archive()));

    do_action( 'ava_after_main_title' ); ?>

    <div class="home-background">
        <div></div>
        <div></div>
        <div>
            <div class="container">
                <h2>THE SHOPPING LIST APP THAT'S SO SAVVY, IT WILL <i>FIND YOU THE BEST PRICES ON EVERY ITEM</i></h2>
            </div>
        </div>
        <a href="#myAnchor" rel="" anchor-tag class="anchorLink"><i></i></a>
    </div>

    <div class="container">
        <?php the_post(); the_content(); ?>
    </div>

    <?php
    get_footer();